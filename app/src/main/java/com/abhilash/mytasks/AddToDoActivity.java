package com.abhilash.mytasks;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

public class AddToDoActivity extends AppCompatActivity {
    Button buttonSave, buttonCancel;
    EditText textTitle, textDescription, textDue, textTimeDue;
    DatePickerDialog dateDue;
    TimePickerDialog timeDue;
    Date dueDate, dueTime;
    boolean isUpdate = false;
    MyToDo simpleTODO;

    private Hashtable<String,String> calendarIdTable;

    private SimpleDateFormat dateFormatter, timeFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo);

        Calendar newCalendar = Calendar.getInstance();

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        timeFormatter = new SimpleDateFormat("HH:mm", Locale.US);

        buttonCancel = findViewById(R.id.buttonCancel);
        buttonSave = findViewById(R.id.buttonSave);

        textTitle = findViewById(R.id.editTextTitle);
        textDescription = findViewById(R.id.editTextDescription);
        textDue = findViewById(R.id.datePickerDueDate);
        textTimeDue = findViewById(R.id.timePickerDueTime);

        textDue.setText(dateFormatter.format(newCalendar.getTime()));
        textTimeDue.setText(timeFormatter.format(newCalendar.getTime()));
        dueDate = dueTime = newCalendar.getTime();

        Intent intentItem = getIntent();
        simpleTODO = (MyToDo) intentItem.getSerializableExtra("data");
        if (simpleTODO != null){
            isUpdate = true;
            textTitle.setText(simpleTODO.getTitle());
            textDescription.setText(simpleTODO.getDescription());
            newCalendar.setTime(simpleTODO.getDue());
            //newCalendar.set(simpleTODO.getDue().getYear(),simpleTODO.getDue().getMonth()-1,simpleTODO.getDue().getDay(),simpleTODO.getDue().getHours(),simpleTODO.getDue().getMinutes());
            textDue.setText(dateFormatter.format(newCalendar.getTime()));
            textTimeDue.setText(timeFormatter.format(newCalendar.getTime()));
            dueDate = dueTime = newCalendar.getTime();
            textDescription.setEnabled(true);
            textDue.setEnabled(true);
            textTimeDue.setEnabled(true);
            buttonSave.setEnabled(true);
//            buttonSave.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            buttonSave.setBackground(getResources().getDrawable(R.drawable.view_transperent_bg));
            ((TextView) findViewById(R.id.textView)).setText("Update TODO");
        }


        textTitle.addTextChangedListener(new TextWatcher() {
                                             @Override
                                             public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                             }

                                             @Override
                                             public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                 if(textTitle.getText().length()>0){
                                                     buttonSave.setEnabled(true);
                                                     textDue.setEnabled(true);
                                                     textDescription.setEnabled(true);
                                                     textTimeDue.setEnabled(true);
//                                                     buttonSave.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                                                     buttonSave.setBackground(getResources().getDrawable(R.drawable.view_transperent_bg));
                                                 }
                                                 else{
                                                     buttonSave.setEnabled(false);
                                                     textDue.setEnabled(false);
                                                     textDescription.setEnabled(false);
                                                     textTimeDue.setEnabled(false);
//                                                     buttonSave.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDisabled));
//                                                     buttonSave.setBackground(getResources().getDrawable(R.drawable.view_transperent_bg));
                                                 }
                                             }

                                             @Override
                                             public void afterTextChanged(Editable s) {
                                             }
                                         }
        );

        textDue.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dateDue.show();
                    }
                }
        );

        textTimeDue.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        timeDue.show();
                    }
                }
        );

        timeDue = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar newDateTime = Calendar.getInstance();
                newDateTime.set(newDateTime.get(Calendar.YEAR), newDateTime.get(Calendar.MONTH), newDateTime.get(Calendar.DAY_OF_MONTH),hourOfDay,minute);
                textTimeDue.setText(timeFormatter.format(newDateTime.getTime()));
                dueTime = newDateTime.getTime();
            }
        },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE),true);


        dateDue = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                textDue.setText(dateFormatter.format(newDate.getTime()));
                dueDate = newDate.getTime();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        buttonCancel.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );

        buttonSave.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String title = textTitle.getText().toString();
                        String description = textDescription.getText().toString();

                        if (title == null || title.isEmpty() || title.trim().equals("")){
                            Toast.makeText(AddToDoActivity.this, "Can not save empty todo task", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Calendar reminderDate = Calendar.getInstance();
                        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(dueDate));
                        int month = Integer.parseInt(new SimpleDateFormat("MM").format(dueDate));
                        int day = Integer.parseInt(new SimpleDateFormat("dd").format(dueDate));
                        int hour = Integer.parseInt(new SimpleDateFormat("HH").format(dueTime));
                        int minutes = Integer.parseInt(new SimpleDateFormat("mm").format(dueTime));
                        reminderDate.set(year,month-1,day,hour,minutes);
                        Date due  =  reminderDate.getTime(); //dueDate; //getDateFromDatePicket(dateDue);

                        boolean operationResult;
                        if(!isUpdate){
                            simpleTODO = new MyToDo(AddToDoActivity.this, title, description, due);
                            operationResult = simpleTODO.Save();
                        }
                        else{
                            simpleTODO.setTitle(title);
                            simpleTODO.setDescription(description);
                            simpleTODO.setDue(due);
                            operationResult = simpleTODO.Update();
                        }
                        if (operationResult) {
                            Toast.makeText(AddToDoActivity.this, "TODO Successfully Saved!", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent("ListViewDataUpdated");
                            LocalBroadcastManager.getInstance(v.getContext()).sendBroadcast(intent);


                            /* ----Start Code: Set Local Notification---- */
                            int id =  simpleTODO.getId().intValue();

                            Calendar notificationReminder = Calendar.getInstance();
                            notificationReminder.set(year,month,day,hour,minutes);
                            Date reminder = notificationReminder.getTime();

                            AlarmManager alarmManager = (AlarmManager) getSystemService(getBaseContext().ALARM_SERVICE);

                            Intent notificationIntent = new Intent(getBaseContext(),NotificationBroadcastReceiver.class);

                            notificationIntent.putExtra("Notification", buildNotification(title,description,id));
                            notificationIntent.putExtra("NotificationDate",reminder);
                            notificationIntent.putExtra("NotificationID",id);

                            PendingIntent broadcast = PendingIntent.getBroadcast(getBaseContext(), id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(System.currentTimeMillis());
                            cal.clear();
                            cal.set(Calendar.YEAR,year);
                            cal.set(Calendar.MONTH,month-1);
                            cal.set(Calendar.DATE,day);
                            cal.set(Calendar.HOUR_OF_DAY,hour);
                            cal.set(Calendar.MINUTE,minutes);

                            Date tempDate = cal.getTime();

                            alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);

                            /* ----End Code: Set Local Notification---- */

                            finish();
                        } else {
                            Toast.makeText(AddToDoActivity.this, "Task not saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
    }

    private Notification buildNotification(String Title, String Description, Integer ID){
        Intent notificationIntent = new Intent(this, MyToDosActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MyToDosActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle("TODO: "+Title);
        if (Description.length()!=0) {
            builder.setContentText(Description);
            builder.setStyle(new Notification.BigTextStyle().bigText(Description));
        }
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setTicker("You have a TODO!");
        builder.setAutoCancel(true);

        Drawable largeIcon = getResources().getDrawable(R.drawable.ic_launcher);
        builder.setLargeIcon((((BitmapDrawable)largeIcon).getBitmap()));

        builder.setContentIntent(pendingIntent);

        Intent actionIntentDone = new Intent(this,NotificationActionReceiver.class);
        actionIntentDone.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        actionIntentDone.setAction("Done");
        actionIntentDone.putExtra("NotificationID",ID);
        PendingIntent actionPeningIntentDone = PendingIntent.getBroadcast(this,ID+1,actionIntentDone,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(R.drawable.ic_done,"Done!",actionPeningIntentDone);

        Intent actionIntentDelete = new Intent(this,NotificationActionReceiver.class);
        actionIntentDelete.setAction("Delete");
        actionIntentDelete.putExtra("NotificationID",ID);
        actionIntentDelete.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent actionPeningIntentDelete = PendingIntent.getBroadcast(this,ID+2,actionIntentDelete,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(R.drawable.ic_delete,"Delete",actionPeningIntentDelete);

        Intent actionIntentClose = new Intent(this,NotificationActionReceiver.class);
        actionIntentClose.setAction("Close");
        actionIntentClose.putExtra("NotificationID",ID);
        actionIntentClose.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent actionPeningIntentClose = PendingIntent.getBroadcast(this,ID+3,actionIntentClose,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(R.drawable.ic_close,"Close",actionPeningIntentClose);


        return builder.build();
    }
}
