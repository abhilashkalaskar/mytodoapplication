package com.abhilash.mytasks;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MyToDosActivity extends AppCompatActivity {

    private ListView mainListView;
    private FloatingActionButton actionButton;
    ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_todos);

        mainListView = findViewById(R.id.myTODOListView);
        actionButton = findViewById(R.id.floatingActionButton);

        loadListView("Title");

        mainListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String item = String.valueOf(parent.getItemAtPosition(position));
                        Toast.makeText(MyToDosActivity.this, "Clicked: "+item, Toast.LENGTH_SHORT).show();
                    }
                }
        );


        actionButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent addIntent = new Intent(MyToDosActivity.this, AddToDoActivity.class);
                        startActivity(addIntent);
                    }
                }
        );
    }

    private void loadListView(String SortColumn){
        listAdapter = new CustomAdapter(this, MyToDo.GetAllTODOs(this,SortColumn));
        mainListView.setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_about) {
//            final Dialog dialog = new Dialog(this);
//            dialog.setContentView(R.layout.dialog_about);
//            dialog.setTitle("About");
//
//            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
//            // if button is clicked, close the custom dialog
//            dialogButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//                }
//            });
//
//            dialog.show();
//            return true;
//        }

        if(id == R.id.action_deleteCompleted){

            //Put up the Yes/No message box
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setTitle("Delete Completed TODO's")
                    .setMessage("Are you sure? This action cannot be reverted.")
                    .setIcon(R.drawable.warning)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (MyToDo.DeleteAllCompleted(MyToDosActivity.this)) {
                                loadListView("Title");
                                Toast.makeText(MyToDosActivity.this, "Completed TODO's deleted!", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(MyToDosActivity.this, "Unable to delete completed TODO's", Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }

        if(id == R.id.action_sortDue){
            loadListView("Due");
        }

        if(id == R.id.action_sortTitle){
            loadListView("Title");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        loadListView("Title");
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("ListViewDataUpdated"));
    }


    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }


    private BroadcastReceiver mMessageReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            loadListView("Title");
        }
    };
}
