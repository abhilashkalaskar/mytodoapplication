package com.abhilash.mytasks;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class CustomAdapter extends ArrayAdapter<MyToDo> {
    private SimpleDateFormat dateFormatter;
    private Context context;

    ImageView imageDelete, imageEdit, imageCompleted;
    TextView tvDoneOrNotDone;

    public CustomAdapter(Context context, List<MyToDo> toDoList) {
        super(context,R.layout.my_custom_row, toDoList);
        this.context = context;
        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm aa", Locale.US);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View customView = inflater.inflate(R.layout.my_custom_row,parent,false);

        MyToDo singleItem = getItem(position);
        TextView itemTitle = customView.findViewById(R.id.tv_title);
        TextView itemDue = customView.findViewById(R.id.tv_due);
        imageEdit = customView.findViewById(R.id.ib_edit);
        imageDelete = customView.findViewById(R.id.ib_delete);
        imageCompleted = customView.findViewById(R.id.ib_completed);
        tvDoneOrNotDone = customView.findViewById(R.id.tv_done_or_notDone);

        imageEdit.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MyToDo singleItem = getItem(position);

                        if (singleItem.IsCompleted()){
                            Toast.makeText(getContext(), "Can not edit completed task.", Toast.LENGTH_SHORT);
                            imageEdit.setEnabled(false);
                            return;
                        }
                        Intent editIntent = new Intent(context, AddToDoActivity.class);
                        editIntent.putExtra("data",singleItem);
                        context.startActivity(editIntent);
                    }
                }
        );

        imageCompleted.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MyToDo singleItem = getItem(position);

                        if(singleItem.IsCompleted()) {
                            singleItem.SetAsNotStarted();
                            tvDoneOrNotDone.setText("Mark as Done");
                            imageCompleted.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_done));
                            Toast.makeText(v.getContext(), singleItem.getTitle() + " set as not completed!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            singleItem.SetAsCompleted();
                            tvDoneOrNotDone.setText("Mark as\nNot Done");
                            imageCompleted.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close));
                            Toast.makeText(v.getContext(), singleItem.getTitle() + " has been completed!", Toast.LENGTH_SHORT).show();
                        }

                        NotificationManager manager = (NotificationManager) v.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.cancel(singleItem.getId().intValue());

                        Intent intent = new Intent("ListViewDataUpdated");
                        LocalBroadcastManager.getInstance(v.getContext()).sendBroadcast(intent);
                    }
                }
        );

        imageDelete.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        /*
                        View parentView = (View) v.getParent();
                        TextView t = (TextView)  parentView.findViewById(R.id.textViewTitle);
                        String s = t.getText().toString();
                        */

                        //Put up the Yes/No message box
                        AlertDialog.Builder builder = new AlertDialog.Builder(CustomAdapter.this.getContext());
                        builder.setTitle("Delete Task")
                            .setMessage("Are you sure? This action cannot be reverted.")
                            .setIcon(R.drawable.warning)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        MyToDo singleItem = getItem(position);
                                        if (singleItem.Delete()) {
                                            Toast.makeText(CustomAdapter.this.getContext(), singleItem.getTitle()+" has been deleted!", Toast.LENGTH_SHORT).show();

                                            NotificationManager manager = (NotificationManager)CustomAdapter.this.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                            manager.cancel(singleItem.getId().intValue());

                                            Intent intent = new Intent("ListViewDataUpdated");
                                            LocalBroadcastManager.getInstance(CustomAdapter.this.getContext()).sendBroadcast(intent);
                                        }
                                        else{
                                            Toast.makeText(CustomAdapter.this.getContext(), "Unable to delete TODO", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
        );

        itemTitle.setText(singleItem.getTitle());
        itemDue.setText("Due: " + dateFormatter.format(singleItem.getDue()));

        if(singleItem.IsCompleted()){
            itemTitle.setPaintFlags(itemTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemDue.setPaintFlags(itemDue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            itemTitle.setEnabled(false);
            itemDue.setEnabled(false);
            imageEdit.setEnabled(false);

            tvDoneOrNotDone.setText("Mark as\nNot Done");
            imageCompleted.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close));

            Resources res = getContext().getResources();
            // TODO: 28/5/20 uncomment
//            Drawable img = res.getDrawable(R.drawable.ic_action_edit_disabled);
//            imageEdit.setImageDrawable(img);
//            img = res.getDrawable(R.drawable.ic_action_undo);
//            imageCompleted.setImageDrawable(img);
        }

        else {
            tvDoneOrNotDone.setText("Mark as Done");
            imageCompleted.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_done));
        }
        /*
        else
        {
           itemTitle.setPaintFlags(itemTitle.getPaintFlags() | (~Paint.STRIKE_THRU_TEXT_FLAG));
           itemDue.setPaintFlags(itemDue.getPaintFlags() | (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
        */

        return customView;
    }
}
